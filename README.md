﻿
![picture](https://cdn2.steamgriddb.com/logo_thumb/154aa6866aefb6f8d0b722621fa71e83.png) 

Doom setup/launch script powered by GZDoom, this is also a set of AUR packages.

The binaries were compiled from the official GZDoom repo
https://github.com/ZDoom/gzdoom

This has no affiliation with ID software and uses original wad files from an archive to preserve the game being kept alive by open source engines

If you want to support ID software please buy the game  from Steam or GOG

[Steam](https://store.steampowered.com/app/2280/DOOM_1993))

[GOG](https://www.gog.com/en/game/doom_1993))

Packages: 

[Doom](https://aur.archlinux.org/packages/doom))

[gzdoom-bin](https://aur.archlinux.org/packages/gzdoom-bin))

 ### Author
  * Corey Bruce
